<?php
// Подключаем код NuSOAP
require_once('./lib/nusoap.php');
// Создаем экземпляр сервера
$server = new soap_server;
// Регистрируем предоставляемый метод
$server->register('hello');
// Определяем метод как функцию PHP
function hello($name) {
    $lenStr = strlen($name);
    //Проверка на длину
    if ($lenStr > 8) return 'So long! Please input string from 1 to 8';

    $servername = "localhost";
    $username = "root";
    $password = "root";
    try {
        $conn = new PDO("mysql:host=$servername;dbname=tz", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e)
    {
        return "Connection failed: " . $e->getMessage();
    }
    //ПРоверить если в мускуле
    $stmt = $conn->prepare("SELECT * FROM `main` WHERE `stroka` = '".$name."'");



    try{
        $stmt->execute();

        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

        if ($result > 0) {

            $result = $stmt->setFetchMode(PDO::FETCH_NUM);
            while ($row = $stmt->fetch()) {
                return 'Count of perestroyka, ' .  $row[2] . ' time of ' . $row[1];
            }
        }
        /* Результатов нет -- делаем что-то другое */
        else {
            $start = microtime(true);
            $fact1 = gmp_fact($lenStr);
            $finish = microtime(true) - $start;

            //Записать в Мускул
            $sql = "INSERT INTO `main` (`stroka`, `timeOf`, `rez`) VALUES ('".$name."', '".$finish."', '".$fact1."')";

            try{
                $conn->exec($sql);
            }catch(PDOException $e)
            {
                return "Connection failed: " . $e->getMessage();
            }
            $res = null;
            $conn = null;
            return 'Count of perestroyka, ' . $fact1 . ' time of ' . $finish;
        }
    }catch(PDOException $e)
    {
        $res = null;
        $conn = null;
        return "Connection failed: " . $e->getMessage();
    }

    $res = null;
    $conn = null;

}
// Используем HTTP-запрос чтобы вызвать сервис.
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>